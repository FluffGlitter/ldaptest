package main

import (
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/FluffGlitter/BobsMods/server"
	"gitlab.com/FluffGlitter/BobsMods/server/session"
)

// appCtx - Context data for the application
type appCtx struct {
	serveAddr      string
	title          string
	logger         *log.Logger
	templates      *template.Template
	sessionManager *session.Manager
}

func (ctx *appCtx) init(title string, addr string, templatePath string) {
	var err error
	ctx.serveAddr = addr
	ctx.title = title

	ctx.logger = log.New(os.Stdout, ctx.title+": ", log.LstdFlags)

	ctx.templates = template.New("Templates")
	ctx.templates.Funcs(
		template.FuncMap{
			"embedTemplate": server.GetTemplateEmbedder(ctx.templates),
		})
	ctx.templates, err = ctx.templates.ParseGlob(templatePath)
	if err != nil {
		ctx.logger.Printf("Unable to parse templates: %v", err)
		return
	}

	cwd, _ := os.Getwd()
	ctx.sessionManager, err = session.NewManager(
		map[string]interface{}{
			"SessionStorage": "SessionStoreFile",
			"SessionStorageOpts": map[string]interface{}{
				"Filename": filepath.Join(cwd, "sessions.json"),
			},
			"IdProvider": "IdProviderRand",
		})
	if err != nil {
		ctx.logger.Fatal(err)
	}
}

func getSessionCookie(w http.ResponseWriter, r *http.Request) *http.Cookie {
	c, err := r.Cookie("session")
	if err != nil {
		return nil
	}
	return c
}

func invalidateSessionCookie(w http.ResponseWriter, c *http.Cookie) {
	c.MaxAge = -1
	http.SetCookie(w, c)
}

func getLoginHandler(ctx *appCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			http.Redirect(w, r, "/", http.StatusMethodNotAllowed)
			return
		}

		if getSessionCookie(w, r) != nil {
			// Already has a session. Redirect to /
			http.Redirect(w, r, r.Referer(), http.StatusFound)
			return
		}

		username := r.FormValue("username")
		password := r.FormValue("password")
		if username == "" || password == "" {
			http.Redirect(w, r, r.Referer(), http.StatusSeeOther)
			return
		}

		session, err := ctx.sessionManager.NewSession()
		if err != nil {
			ctx.logger.Print(err)
			http.Redirect(w, r, "/", http.StatusInternalServerError)
			return
		}
		id, exists := session.ID().(string)
		if !exists {
			ctx.logger.Print(err)
			http.Redirect(w, r, "/", http.StatusInternalServerError)
			return
		}
		session.Set("username", username)
		session.Set("authenticated", true) // TODO

		cookie := &http.Cookie{
			Name:    "session",
			Value:   id,
			Expires: time.Now().Add(24 * time.Hour),
			//MaxAge:  int((time.Duration(24) * time.Hour).Seconds()),
			// TODO: secure it and set an expire etc
		}

		http.SetCookie(w, cookie)
		ctx.logger.Printf("[SESSION COOKIE]: %+v", cookie)

		err = ctx.sessionManager.PutSession(session)
		if err != nil {
			ctx.logger.Print(err)
			http.Redirect(w, r, "/", http.StatusInternalServerError)
			return
		}
		http.Redirect(w, r, r.Referer(), http.StatusSeeOther)
	})
}

func getHomeHandler(ctx *appCtx) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		templateContext := map[string]interface{}{
			"Title": "LDAP Test",
			"Body":  "index",
		}

		cookie := getSessionCookie(w, r)
		if cookie != nil {
			idString := cookie.Value
			session, err := ctx.sessionManager.GetSession(idString)
			if err != nil {
				invalidateSessionCookie(w, cookie)
				ctx.logger.Print(err)
				http.Redirect(w, r, "/", http.StatusForbidden)
				return
			}
			authenticated, authExists := session.Get("authenticated").(bool)
			if !authExists {
				authenticated = false
			}
			username, userExists := session.Get("username").(string)
			if !userExists {
				username = ""
			}
			if authenticated && username != "" {
				templateContext["Username"] = username
			}
		}

		server.GetBasicTemplateHandler(ctx.templates, "stdLayout", templateContext)(w, r)
	})
}

func main() {
	ctx := &appCtx{}
	ctx.init("ldaptest", "localhost:8080", "templates/*")

	server.Handle("/login", server.Adapt(getLoginHandler(ctx),
		server.LogRequests(ctx.logger)))
	server.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	server.Handle("/", server.Adapt(getHomeHandler(ctx),
		server.LogRequests(ctx.logger)))

	server.ListenAndServe(ctx.logger, ctx.serveAddr, nil, true)
}
